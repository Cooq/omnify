#!/bin/bash
if [[ $* =~ --css ]]; then
  p=$*;
  source css.sh ${p:5}
elif [[ $* =~ --html ]]; then
  p=$*;
  source html.sh ${p:6}
fi
