#!/bin/bash

function minify {
  a=$( tr -s ' ' < $1 | tr "\n" " ")
  echo $a | sed -e 's/> </></g'
}
if [ $# -eq 0 ]; then
  echo "not enough arguments"
elif [ $# -eq 1 ]; then
  if [[ $1 =~ (.+)\.([A-Za-z]+) ]]; then
    echo "minified on "${BASH_REMATCH[1]}'.min.'${BASH_REMATCH[2]};
    minify $1 > ${BASH_REMATCH[1]}'.min.'${BASH_REMATCH[2]}
  fi
elif [ $# -eq 2 ]; then
  minify $1 > $2
else
  echo "Too many arguments"
fi
