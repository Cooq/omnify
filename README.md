# Omnify

## Requirements

- bash

## Scripts

### _One Script to run them all_ (aka main.sh)
#### CSS

```
./main.sh --css input [ouput]
```

#### HTML
```
./main.sh --html input [ouput]
```

### CSS minifier (aka css.sh)

```
./css.sh input [output]
```
If there is not output, the file produced will be named filename**.min**.css.  
Usually, reduce by _20%_ of the file.

### HTML minifier (aka html.sh)
```
./html.sh input [output]
```
If there is not output, the file produced will be named filename**.min**.html.  
Usually, reduce by _20%_ of the file.

### JS minifer *Maybe Someday*
